class CustomerService {
    customers = [
        {
            id: 1,
            name: 'Petar',
            lastName: 'Petrović',
            age: 25,
            products: [{
              id: 1,
              name: 'Usisivač',
              description: 'Lorem ipsum dolor',
              price:250
          },
          {
              id: 2,
              name: 'Pegla',
              description: 'Lorem ipsum dolor',
              price: 50
          }]
        },
        {
            id: 2,
            name: 'Marko',
            lastName: 'Marković',
            age: 25,
            products:[
              {
                id: 1,
                name: 'Usisivač',
                description: 'Lorem ipsum dolor',
                price:250
            },
            {
                id: 2,
                name: 'Pegla',
                description: 'Lorem ipsum dolor',
                price: 50
            },
            {
                id: 3,
                name: 'Fen za kosu',
                description: 'Lorem ipsum dolor',
                price:30
            }
            ]
        },
        {
            id: 3,
            name: 'Jovan',
            lastName: 'Jovanović',
            age: 25,
            products:[
              {
                id: 3,
                name: 'Fen za kosu',
                description: 'Lorem ipsum dolor',
                price:30
            }
            ]
        }
    ]


  
    getAll() {
      return [...this.customers];
    }
  
    get(id) {
      // send request to get video with id from server through http client
      return this.customers.filter(customer=> customer.id===id)
    }
  
    // data = {title: 'cao', description: 'asd'}
    create(data) {

      const ids = this.customers.map(customer => customer.id);
      const max = Math.max(...ids);

    const newCustomer = {
        id: max+1,
        ...data
    }

    this.customers = [...this.customers, newCustomer]
    return newCustomer;
        
    }
  
    delete(id) {
      // izbrisete video iz liste 'this.videos'
      const index = this.customers.findIndex(customer=>customer.id===id)

      if(index===-1){
        return false
      }

      this.customers.splice(index,1)
      return true
     
    }
  
    update(id, data) {
  
    }
  }
  
  export default new CustomerService();
  