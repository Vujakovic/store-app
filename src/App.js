import './App.css';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Nav from './components/Nav';
import AppCustomers from './pages/AppCustomers';
import AppProducts from './pages/AppProducts';
import LatestPurchases from './pages/LatestPurchases';


function App() {
  return (
    <div className="App">
        <Router>
            <Nav/>
          <div className="container">
            <Switch>
              <Route path='/customers' exact>
                <AppCustomers/>
              </Route>

              <Route path='/customers/:id' exact>
                <LatestPurchases/>
              </Route>

              <Route path='/products' exact>
                <AppProducts/>
              </Route>

            </Switch>

          </div>
        </Router>
    </div>
  );
}

export default App;
