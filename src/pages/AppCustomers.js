import React, { useState} from 'react'
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import CustomerService from '../service/CustomerService'

function AppCustomers() {


    const [customers, setCustomers] = useState(CustomerService.getAll());

    // ADD CUSTOMER
    const [name,setName] = useState()
    const [lastName,setLastName] = useState()
    const [age,setAge] = useState()

    const addCustomer = (event) => {
        event.preventDefault()
        const data = {name, lastName, age}
        
        console.log(data);
        const newUser = CustomerService.create(data)
        setCustomers([...customers, newUser])
    }
        

    const deleteCustomer = (id)=>{
        const isDeleted= CustomerService.delete(id)

        if(isDeleted){
            setCustomers(customers.filter(customer=>customer.id!==id))
        }
    }
    

  return (
      <div className="customerList">
          <h1>Add new customer</h1>
        <div className="addCustomer row">
            

            <form className='d-block col-md-6' onSubmit={addCustomer}>
                <div className="form-group">
                    <label htmlFor="name">Name:</label>
                    <input type="text" 
                    className="form-control" 
                    placeholder="Name" 
                    id="name"
                    onChange={e => setName(e.target.value)}/>
                </div>

                <div className="form-group">
                    <label htmlFor="lastName">Last Name:</label>
                    <input type="text" 
                    className="form-control" 
                    placeholder="Last name" 
                    id="lastName"
                    onChange={e => setLastName(e.target.value)}/>
                </div>

                <div className="form-group">
                    <label htmlFor="lastName">Age:</label>
                    <input type="number" 
                    className="form-control" 
                    placeholder="Last name" 
                    id="lastName"
                    onChange={e => setAge(e.target.value)}/>
                </div>

                
                <button type="submit" className="btn btn-primary">Add Customer</button>
            </form>
        </div>


        <h2>Customers list:</h2>

        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Age</th>
                    <th>Latest Purchase</th>
                    <th>Delete</th>
                </tr>
            </thead>

            <tbody>
                {customers.map(customer=>(
                    <tr key={customer.id}> 
                        <td>{customer.name}</td>
                        <td>{customer.lastName}</td>
                        <td>{customer.age}</td>
                        <td><Link to={`/customers/${customer.id}`}>Latest purchase</Link></td>
                        <td><button className="btn btn-danger" onClick={()=>deleteCustomer(customer.id)}>
                            Delete
                            </button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
      </div>
  )
}

export default AppCustomers