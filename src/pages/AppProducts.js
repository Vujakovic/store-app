import React, { useState } from 'react'
import ProductService from '../service/ProductService'


function AppProducts() {

    const [products, setProducts] = useState(ProductService.getAll());
    const pocetniNiz = ProductService.getAll()

    const [inputValue, setInputValue] =useState()

    const filterProducts = (e) => {
        
        setProducts(pocetniNiz.filter(product=>product.name.toLowerCase().includes(e.toLowerCase())))
        
        
    }

  return (
    <div className='productsList'>
         <h2>Products list:</h2>

<div className="sortByInput">
<div className="form-group">
  <label htmlFor="usr">Search products by name</label>
  <input type="text" className="form-control" id="name" onChange={e => filterProducts(e.target.value)}/>
</div>

</div>
<table className="table table-striped">
    <thead>
        <tr>
            <th>Product Name</th>
            <th>Description</th>
            <th>Price</th>
        </tr>
    </thead>

    <tbody>
        {products.map(product=>(
            <tr key={product.id}> 
                <td>{product.name}</td>
                <td>{product.description}</td>
                <td>$ {product.price}</td>

            </tr>
        ))}
    </tbody>
</table>
    </div>
  )
}

export default AppProducts