import React, { useState } from 'react'
import { useParams } from 'react-router-dom'
import CustomerService from '../service/CustomerService'

function LatestPurchases() {

    const{id} = useParams()

    const [customer, setCustomer] = useState(CustomerService.get(parseInt(id))[0])

    
  return (
    <div className="latestPurchases">
        <div className="customer-data ">
            <div className="card p-5 mb-2">

            <h4>Customer details:</h4>
            <span className="d-block">Name: <strong>{customer.name}</strong></span>
            <span className="d-block">Last name: <strong>{customer.lastName}</strong></span>
            <span className="d-block">Age: <strong>{customer.age}</strong></span>
            </div>

            <div className="card p-2">
                <h4>Purchased products:</h4>
                <ul>
                    {customer.products.map(product=>(
                        <li>{`${product.name} - ${product.description}`} <strong>$ {product.price}</strong></li>
                    ))}
                </ul>
            </div>

        </div>

        
    </div>
  )
}

export default LatestPurchases