import React from 'react'
import {Link} from 'react-router-dom'

function Nav() {
  return (
    <div>
        <ul className="nav bg-info">
            <li className="nav-item">
                <Link to='/customers' className="nav-link">Customers</Link>
            </li>
            <li className="nav-item">
                <Link to='/products' className="nav-link">Products</Link>
            </li>
        </ul>
    </div>
  )
}

export default Nav